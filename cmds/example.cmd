# Required modules (will automatically load calc, asyn, stream and autosave)
require rssmx100a,1.0.0
require essioc

# Signal Generator IP
epicsEnvSet("IP_ADDR","rflab-sma100a.cslab.esss.lu.se")

# PV prefix
epicsEnvSet("P", "LabS-RFLab:")
epicsEnvSet("R", "RFS-SG-001:")

iocshLoad(${rssmx100a_DIR}rssma100a.iocsh, "P=${P},R=${R},IP_ADDR=${IP_ADDR}")

iocInit()
